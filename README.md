Pelmar Engineering has been a trusted supplier of industrial, municipal and commercial products since 1982. We offer a wide range of solutions, including Cabinet Coolers, Vortex Cooling Solutions, Automatic Water Filtration Systems, Strainers, Bag Filters, Air Release Valves & Water Control Valves.


Address: 8-445 Midwest Rd, Scarborough, ON M1P 4Y9, Canada

Phone: 416-288-1736

Website: http://www.pelmareng.com
